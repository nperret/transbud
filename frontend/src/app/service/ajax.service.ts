import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Operation } from '../entity/operation';
import { Account } from '../entity/account';

@Injectable({
  providedIn: 'root'
})
export class AjaxService {

  private urlOperation = 'http://localhost:8080/api/operation/'
  private urlAccount = 'http://localhost:8080/api/user/account/'

  constructor(private http:HttpClient) { }

  public getAllOperation():Observable<Operation[]>
  {
    return this.http.get<Operation[]>(this.urlOperation)
  }

  public addOneOperation(operation:Operation, id):Observable<Operation>
  {
    return this.http.post<Operation>(`http://localhost:8080/api/user/account/${id}/operation/`,operation)
  }

  public removeOneOperation(operation:Operation, idAccount:number):Observable<any>
  {
  
    return this.http.delete(`http://localhost:8080/api/user/account/${idAccount}/operation/${operation.id}`)
  }

  public getAllAccount():Observable<Account[]>
  {
    return this.http.get<Account[]>(this.urlAccount);
  }

  public getOneAccount(id:number):Observable<Account>
  {
    return this.http.get<Account>(`${this.urlAccount}${id}`);
  }

  public getAllOperationByAccount(id) {

    return this.http.get<Operation[]>(`http://localhost:8080/api/user/account/${id}/operation`);
    
  }
}
