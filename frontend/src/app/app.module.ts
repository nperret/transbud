import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BannerComponent } from './banner/banner.component';
import { FooterComponent } from './footer/footer.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { AccountComponent } from './account/account.component';
import { RegistrationComponent } from './form/registration/registration.component';
import { AccountCreationComponent } from './form/account-creation/account-creation.component';
import { DateFromApiPipe } from './pipes/date-from-api.pipe';

const routes:Routes = [
  {path:'',component: HomeComponent},
  {path: 'user/:id', component: UserComponent},
  {path: 'user/account/:id', component: AccountComponent}


]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BannerComponent,
    FooterComponent,
    AboutComponent,
    HomeComponent,
    UserComponent,
    AccountComponent,
    RegistrationComponent,
    AccountCreationComponent,
    DateFromApiPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
