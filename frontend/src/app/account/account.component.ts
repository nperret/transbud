import { Component, OnInit } from '@angular/core';
import { Operation } from '../entity/operation';
import { Account } from '../entity/account';
import { AjaxService } from '../service/ajax.service';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment'

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  public operations: Operation[] = [];
  newOperation: Partial<Operation> = { "montant": 0 };

  public accounts: Array<Account> = [];

  public account: Account;

  public id: number = 0;

  // sum of operation by type
  public debit = 0;
  public stat = {
    "debit": 0,
    "credit": 0
  }

  constructor(route: ActivatedRoute, private service: AjaxService) {
    route.params.subscribe(data => this.id = +data['id']);
  }

  ngOnInit() {


    this.service.getAllOperationByAccount(this.id).subscribe(
      data => {
        this.operations = data;
        console.log(this.operations);
        this.computeStat();

      },
      error => console.error(error.message),
      () => console.info(`findAll() operation success for id : ${this.id}`)
    )

    this.service.getAllAccount().subscribe(
      data => this.accounts = data,
      error => console.error(error.message),
      () => console.info("findAll() account success")

    )
    //console.log(this.accounts);

    this.service.getOneAccount(this.id).subscribe(
      data => this.account = data,
      error => console.error(`getOneAccount error : ${error}`),
      () => console.info(`getOne account success for id : ${this.id}`)
    )
  }

  public add(operation) {

    operation.date = Date.now();


    console.log(operation);


    this.service.addOneOperation(operation, this.id).subscribe(
      () => this.ngOnInit(),
      error => console.error(`Add One operation : ${error.message}`),
      () => console.info(`add an operation name:${operation.montant}`))
  }

  public remove(operation) {

    this.service.removeOneOperation(operation, this.id).subscribe(
      () => this.ngOnInit(),
      error => console.error(error.mesage),
      () => console.info(`remove operation id : ${operation.id}`)
    );
  }

  public computeStat() {

    for (const operation of this.operations) {
      if(operation.montant < 0){
        this.stat["debit"] += operation.montant; 
      }
      else {
        this.stat['credit'] += operation.montant;
      }
    }
    
  }




}
