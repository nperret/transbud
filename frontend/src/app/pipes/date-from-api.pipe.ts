import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFromApi'
})
export class DateFromApiPipe implements PipeTransform {

  transform(timestamp: number, format: string = "sec"): any {
    if(format === "sec"){
      return timestamp*1000;
    }
  }

}
