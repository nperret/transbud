export interface Operation {
  id:number;
  montant:number;
  account:Account;
  date:Date;
}
