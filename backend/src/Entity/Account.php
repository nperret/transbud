<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $banque;

    /**
     * @ORM\Column(type="integer")
     */
    private $solde;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Operation", mappedBy="account", orphanRemoval=true, cascade={"persist"})
     */
    private $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName() : ? string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    public function getBanque() : ? string
    {
        return $this->banque;
    }

    public function setBanque(string $banque) : self
    {
        $this->banque = $banque;

        return $this;
    }

    public function getSolde() : ? int
    {
        return $this->solde;
    }

    public function setSolde(int $solde) : self
    {
        $this->solde = $solde;

        return $this;
    }

    /**
     * @return Collection|Operation[]
     */
    public function getOperations() : Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation) : self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations[] = $operation;
            $operation->setAccount($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation) : self
    {
        if ($this->operations->contains($operation)) {
            $this->operations->removeElement($operation);
            // set the owning side to null (unless already changed)
            if ($operation->getAccount() === $this) {
                $operation->setAccount(null);
            }
        }

        return $this;
    }

    public function computeSolde($operation, $remove = false)
    {
        $solde = $this->solde;

        if (!$remove) {
            $this->solde += $operation->getMontant();
        }
        else
        {
            $this->solde -= $operation->getMontant();
        }

        return $this;


    }
}
